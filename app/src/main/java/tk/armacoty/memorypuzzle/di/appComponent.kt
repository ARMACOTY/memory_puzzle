package tk.armacoty.memorypuzzle.di

val appComponent = listOf(
    databaseModule,
    viewModelModule
)
