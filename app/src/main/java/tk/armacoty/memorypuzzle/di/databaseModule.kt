package tk.armacoty.memorypuzzle.di

import android.app.Application
import androidx.room.Room
import org.koin.dsl.module
import tk.armacoty.memorypuzzle.model.database.AppDatabase

private const val DATABASE_NAME = "app.db"

val databaseModule = module {
    single {
        room(get())
    }

    single {
        gameResultDao(get())
    }
}

fun room(app: Application) = Room
    .databaseBuilder(app, AppDatabase::class.java, DATABASE_NAME)
    .build()

fun gameResultDao(appDatabase: AppDatabase) = appDatabase.gameResultDao()
