package tk.armacoty.memorypuzzle.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import tk.armacoty.memorypuzzle.model.entity.Difficulty
import tk.armacoty.memorypuzzle.model.entity.GameResult
import tk.armacoty.memorypuzzle.ui.screen.game.GameViewModel
import tk.armacoty.memorypuzzle.ui.screen.game_result.GameResultViewModel
import tk.armacoty.memorypuzzle.ui.screen.history.HistoryViewModel

val viewModelModule = module {
    viewModel { (difficulty: Difficulty) ->
        GameViewModel(difficulty)
    }

    viewModel { (gameResult: GameResult) ->
        GameResultViewModel(gameResult)
    }

    viewModel {
        HistoryViewModel(get())
    }
}
