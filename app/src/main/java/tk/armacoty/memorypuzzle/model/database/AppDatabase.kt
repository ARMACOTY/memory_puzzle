package tk.armacoty.memorypuzzle.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import tk.armacoty.memorypuzzle.model.database.converter.Converters
import tk.armacoty.memorypuzzle.model.database.dao.GameResultDao
import tk.armacoty.memorypuzzle.model.entity.GameResult

@Database(
    entities = [GameResult::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun gameResultDao(): GameResultDao
}
