package tk.armacoty.memorypuzzle.model.database.converter

import android.annotation.SuppressLint
import androidx.room.TypeConverter
import java.time.LocalDateTime

class Converters {
    @SuppressLint("NewApi")
    @TypeConverter
    fun toLocalDateTime(value: String): LocalDateTime {
        return LocalDateTime.parse(value)
    }

    @TypeConverter
    fun fromLocalDateTime(value: LocalDateTime): String {
        return value.toString()
    }
}
