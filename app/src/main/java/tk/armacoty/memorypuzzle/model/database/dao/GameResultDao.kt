package tk.armacoty.memorypuzzle.model.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import tk.armacoty.memorypuzzle.model.entity.GameResult

@Dao
interface GameResultDao {
    @Query("SELECT * FROM gameResult")
    fun getAll(): List<GameResult>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(gameResult: GameResult)

    @Update
    fun update(gameResult: GameResult)

    @Delete
    fun delete(gameResult: GameResult)
}
