package tk.armacoty.memorypuzzle.model.entity

import kotlin.random.Random

object CardSymbols {
    private val symbols = arrayListOf(
        "\uD83D\uDE3A", // 😺
        "\uD83E\uDD1A", // 🤚
        "\uD83D\uDD76", // 🕶
        "☂️", // ☂️
        "\uD83C\uDF85", // 🎅
        "\uD83E\uDD8A", // 🦊
        "\uD83D\uDC36", // 🐶
        "\uD83D\uDC3C", // 🐼
        "\uD83D\uDC28", // 🐨
        "\uD83D\uDC38", // 🐸

        "\uD83D\uDC12", // 🐒
        "\uD83D\uDC27", // 🐧
        "\uD83E\uDD89", // 🦉
        "\uD83D\uDC24", // 🐤
        "\uD83C\uDF1D", // 🌝
        "\uD83E\uDD9D", // 🦝
        "⛄", // ⛄
        "\uD83C\uDF4E", // 🍎
        "\uD83C\uDF4A", // 🍊
        "\uD83C\uDF4B", // 🍋

        "\uD83C\uDF4C", // 🍌
        "\uD83C\uDF49", // 🍉
        "\uD83C\uDF45", // 🍅
        "\uD83C\uDF46", // 🍆
        "\uD83E\uDD56", // 🥖
        "\uD83C\uDF2D", // 🌭
        "\uD83C\uDF54", // 🍔
        "\uD83C\uDF55", // 🍕
        "\uD83E\uDDC1", // 🧁
        "\uD83C\uDF7A", // 🍺

        "⚽", // ⚽️
        "\uD83C\uDFC0", // 🏀
        "\uD83C\uDFC8", // 🏈
        "⚾", // ⚾️
        "\uD83C\uDFB1", // 🎱
        "\uD83C\uDFB2", // 🎲
        "♟", // ♟
        "\uD83C\uDFAE", // 🎮
        "\uD83C\uDFB9", // 🎹
        "\uD83C\uDFC6", // 🏆

        "\uD83D\uDE95", // 🚕
        "\uD83D\uDE91", // 🚑
        "\uD83D\uDE92", // 🚒
        "\uD83D\uDE9C", // 🚜
        "\uD83D\uDEF4", // 🛴
        "\uD83D\uDEA6", // 🚦
        "\uD83D\uDEF0", // 🛰
        "✈", // ✈️
        "\uD83D\uDE80", // 🚀
        "\uD83D\uDDFF", // 🗿

        "\uD83D\uDCF1", // 📱
        "\uD83D\uDCBB", // 💻
        "\uD83D\uDCF7", // 📷
        "\uD83D\uDCA1", // 💡
        "\uD83D\uDC8E", // 💎
        "\uD83D\uDD28", // 🔨
        "\uD83E\uDDF8", // 🧸
        "\uD83D\uDD12", // 🔒
        "\uD83D\uDD11", // 🔑
        "\uD83D\uDECF", // 🛏

        "☢", // ☢️
        "❤", // ❤️
        "\uD83D\uDCAF", // 💯
        "Ⓜ", // Ⓜ️
        "\uD83C\uDCCF", // 🃏
        "\uD83D\uDD50", // 🕐
        "\uD83D\uDD21", // 🔡
        "♻", // ♻️
        "\uD83C\uDF10", // 🌐
        "⛔", // ⛔️

        "\uD83C\uDF83", // 🎃
        "\uD83D\uDC7D" // 👽
    )

    fun give(difficulty: Difficulty): ArrayList<String> {
        val n = difficulty.value * difficulty.value / 2

        val result = arrayListOf<String>()

        while (result.size < n) {
            val index: Int = Random.nextInt(symbols.size)
            val symbol: String = symbols[index]

            if (!result.contains(symbol)) {
                result.add(symbol)
            }
        }

        return result
    }
}
