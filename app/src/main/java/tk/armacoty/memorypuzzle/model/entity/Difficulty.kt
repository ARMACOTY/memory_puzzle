package tk.armacoty.memorypuzzle.model.entity

enum class Difficulty(val value: Int) {
    EASY(4),
    MEDIUM(6),
    HARD(8)
}
