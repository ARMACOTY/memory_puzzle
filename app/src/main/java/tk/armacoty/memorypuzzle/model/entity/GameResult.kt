package tk.armacoty.memorypuzzle.model.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue
import java.time.LocalDateTime

@Parcelize
@Entity
data class GameResult(
    @PrimaryKey
    val time: @RawValue LocalDateTime,

    val difficulty: Int,

    val moves: Int
) : Parcelable
