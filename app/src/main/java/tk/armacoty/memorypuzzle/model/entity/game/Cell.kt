package tk.armacoty.memorypuzzle.model.entity.game

class Cell(
    val value: String
) {
    var state: CellState = CellState.CLOSED
}
