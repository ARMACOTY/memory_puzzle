package tk.armacoty.memorypuzzle.model.entity.game

enum class CellState(val value: Int) {
    OPENED(1),
    SELECTED(2),
    CLOSED(3),
    WRONG(4),
    RIGHT(5),
}
