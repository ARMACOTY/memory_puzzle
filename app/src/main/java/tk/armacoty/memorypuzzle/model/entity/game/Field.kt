package tk.armacoty.memorypuzzle.model.entity.game

import tk.armacoty.memorypuzzle.model.entity.CardSymbols
import tk.armacoty.memorypuzzle.model.entity.Difficulty

class Field(
    val difficulty: Difficulty,
    val onGameOver: (moves: Int) -> Unit,
) {
    val rows: ArrayList<Row>

    private var moves = 0

    init {
        val cardValues = shuffleCells(difficulty)
        rows = arrayListOf()
        for (r in 0 until difficulty.value) {
            val cells = arrayListOf<Cell>()
            for (c in 0 until difficulty.value) {
                val index = r * difficulty.value + c
                val cell = Cell(cardValues[index])
                cells.add(cell)
            }
            val row = Row(cells)
            rows.add(row)
        }
    }

    fun onSelect(row: Int, col: Int) {
        val thisCell = rows[row].cells[col]
        if (thisCell.state == CellState.RIGHT ||
            thisCell.state == CellState.OPENED ||
            thisCell.state == CellState.SELECTED
        ) return

        if (haveSelectedCell()) {
            moves++

            val anotherCell = anotherCell()
            if (anotherCell != null) {
                if (anotherCell.value == thisCell.value) {
                    thisCell.state = CellState.RIGHT
                    anotherCell.state = CellState.RIGHT
                } else {
                    thisCell.state = CellState.WRONG
                    anotherCell.state = CellState.WRONG
                }
            }
        } else {
            rows[row].cells[col].state = CellState.SELECTED
        }
    }

    fun handleAutoUpdate() {
        clearWrong()
        clearSolved()
        if (isSolved())
            this.onGameOver(moves)
    }

    private fun isSolved(): Boolean {
        for (row in rows)
            for (cell in row.cells)
                if (cell.state != CellState.OPENED)
                    return false
        return true
    }

    private fun clearWrong() {
        for (row in rows)
            for (cell in row.cells)
                if (cell.state == CellState.WRONG)
                    cell.state = CellState.CLOSED
    }

    private fun clearSolved() {
        for (row in rows)
            for (cell in row.cells)
                if (cell.state == CellState.RIGHT)
                    cell.state = CellState.OPENED
    }

    private fun haveSelectedCell(): Boolean {
        for (row in rows)
            for (cell in row.cells)
                if (cell.state == CellState.SELECTED)
                    return true
        return false
    }

    private fun anotherCell(): Cell? {
        for (row in rows)
            for (cell in row.cells)
                if (cell.state == CellState.SELECTED)
                    return cell
        return null
    }

    companion object {
        private fun shuffleCells(difficulty: Difficulty): ArrayList<String> {
            val cardValues = CardSymbols.give(difficulty)
            val cards = arrayListOf<String>()
            with(cards) {
                addAll(cardValues)
                addAll(cardValues)
                shuffle()
            }
            return cards
        }
    }
}
