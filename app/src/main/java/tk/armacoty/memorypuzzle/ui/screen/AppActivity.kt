package tk.armacoty.memorypuzzle.ui.screen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import tk.armacoty.memorypuzzle.R

class AppActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)
    }
}
