package tk.armacoty.memorypuzzle.ui.screen.game

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import tk.armacoty.memorypuzzle.R
import tk.armacoty.memorypuzzle.databinding.GameFragmentBinding
import tk.armacoty.memorypuzzle.model.database.dao.GameResultDao
import tk.armacoty.memorypuzzle.model.entity.GameResult
import tk.armacoty.memorypuzzle.ui.screen.game.adapter.RowAdapter
import java.time.LocalDateTime
import kotlin.concurrent.thread
import kotlin.math.min

class GameFragment : Fragment() {
    private val gameViewModel: GameViewModel by viewModel {
        parametersOf(navArgs<GameFragmentArgs>().value.difficulty)
    }

    private val gameResultDao: GameResultDao by inject()

    private lateinit var binding: GameFragmentBinding

    @SuppressLint("NewApi")
    private fun toGameResult(moves: Int) {
        val gameResult = GameResult(
            time = LocalDateTime.now(),
            difficulty = gameViewModel.difficulty.value,
            moves = moves
        )

        findNavController().navigate(
            GameFragmentDirections.actionGameEnded(
                gameResult
            )
        )

        thread(true) {
            gameResultDao.insert(gameResult)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.game_fragment,
            container,
            false
        )

        with(binding) {
            val adapter = RowAdapter()

            rows.adapter = adapter

            val size = when {
                container != null -> min(container.width, container.height)
                else -> 0
            }

            rows.minimumWidth = size
            rows.minimumHeight = size

            gameViewModel.field.observe(
                viewLifecycleOwner
            ) { field ->
                field?.let {
                    with(adapter) {
                        data = field.rows
                        cellSize = size / gameViewModel.difficulty.value
                        onTap = { row, col ->
                            gameViewModel.onSelect(row, col)
                        }
                    }
                }
            }
        }

        gameViewModel.onGameOver = { moves ->
            toGameResult(moves)
        }

        return binding.root
    }
}
