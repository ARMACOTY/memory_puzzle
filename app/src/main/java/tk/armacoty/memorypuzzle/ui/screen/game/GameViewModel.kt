package tk.armacoty.memorypuzzle.ui.screen.game

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import tk.armacoty.memorypuzzle.model.entity.Difficulty
import tk.armacoty.memorypuzzle.model.entity.game.Field

class GameViewModel(
    val difficulty: Difficulty
) : ViewModel() {
    private val _field = MutableLiveData<Field>()
    val field: LiveData<Field>
        get() = _field

    private var gameOver = false

    lateinit var onGameOver: (moves: Int) -> Unit

    private val buttonChannel = Channel<Unit>()

    init {
        _field.value = Field(difficulty) { moves: Int ->
            if (!gameOver) {
                onGameOver(moves)
                gameOver = true
            }
        }
        viewModelScope.launch(Dispatchers.IO) {
            buttonChannel
                .consumeAsFlow()
                .debounce(1000)
                .collect {
                    viewModelScope.launch(Dispatchers.Main) {
                        _field.value?.handleAutoUpdate()
                        _field.postValue(_field.value)
                    }
                }
        }
    }

    fun onSelect(row: Int, col: Int) {
        _field.value?.handleAutoUpdate()
        _field.value?.onSelect(row, col)
        _field.postValue(_field.value)
        viewModelScope.launch(Dispatchers.IO) {
            buttonChannel.send(Unit)
        }
    }
}
