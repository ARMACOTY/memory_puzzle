package tk.armacoty.memorypuzzle.ui.screen.game.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tk.armacoty.memorypuzzle.R
import tk.armacoty.memorypuzzle.model.entity.game.Cell
import tk.armacoty.memorypuzzle.model.entity.game.CellState

class CellAdapter : RecyclerView.Adapter<CellAdapter.CellViewHolder>() {
    var data = listOf<Cell>()
        @SuppressLint("NotifyDataSetChanged")
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onTap: (col: Int) -> Unit = { _ ->
    }

    var cellSize = 0

    override fun getItemViewType(position: Int) =
        data[position].state.value

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        from(parent, viewType)

    override fun onBindViewHolder(holder: CellViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item, position, cellSize, onTap)
    }

    override fun getItemCount() = data.size

    inner class CellViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val cellText: TextView = itemView.findViewById(R.id.cell_text)
        private val cellLayout: LinearLayout = itemView.findViewById(R.id.cell_layout)

        fun bind(
            item: Cell,
            index: Int,
            cellSize: Int,
            onTap: (col: Int) -> Unit
        ) {
            if (item.state == CellState.SELECTED ||
                item.state == CellState.WRONG ||
                item.state == CellState.RIGHT
            ) {
                cellText.text = item.value
            }

            with(cellLayout) {
                minimumHeight = cellSize
                minimumWidth = cellSize
                setOnClickListener {
                    onTap(index)
                }
            }
        }
    }

    private fun from(parent: ViewGroup, viewType: Int): CellViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(
            when (viewType) {
                CellState.OPENED.value -> R.layout.game_cell_opened_layout
                CellState.SELECTED.value -> R.layout.game_cell_selected_layout
                CellState.CLOSED.value -> R.layout.game_cell_closed_layout
                CellState.RIGHT.value -> R.layout.game_cell_selected_layout
                else -> R.layout.game_cell_selected_layout
            },
            parent,
            false
        )

        return CellViewHolder(view)
    }
}
