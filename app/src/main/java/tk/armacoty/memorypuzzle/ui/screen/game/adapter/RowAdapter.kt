package tk.armacoty.memorypuzzle.ui.screen.game.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tk.armacoty.memorypuzzle.R
import tk.armacoty.memorypuzzle.model.entity.game.Row

class RowAdapter : RecyclerView.Adapter<RowAdapter.RowViewHolder>() {
    var data = listOf<Row>()
        @SuppressLint("NotifyDataSetChanged")
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onTap: (row: Int, col: Int) -> Unit = { _, _ ->
    }

    var cellSize = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        from(parent)

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item, position, cellSize, onTap)
    }

    override fun getItemCount() = data.size

    class RowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val cells: RecyclerView = itemView.findViewById(R.id.cells)

        fun bind(
            item: Row,
            index: Int,
            size: Int,
            onTap: (row: Int, col: Int) -> Unit
        ) {
            val adapter = CellAdapter()
            with(adapter) {
                data = item.cells
                cellSize = size
                this.onTap = { col ->
                    onTap(index, col)
                }
            }
            cells.adapter = adapter
            cells.minimumHeight = size
        }
    }

    private fun from(parent: ViewGroup): RowViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(
            R.layout.game_row_layout,
            parent,
            false
        )

        return RowViewHolder(view)
    }
}
