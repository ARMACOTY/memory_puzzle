package tk.armacoty.memorypuzzle.ui.screen.game_result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import tk.armacoty.memorypuzzle.R
import tk.armacoty.memorypuzzle.databinding.GameResultFragmentBinding

class GameResultFragment : Fragment() {
    private val gameResultViewModel: GameResultViewModel by viewModel {
        parametersOf(navArgs<GameResultFragmentArgs>().value.gameResult)
    }

    private lateinit var binding: GameResultFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.game_result_fragment,
            container,
            false
        )

        binding.apply {
            close.setOnClickListener {
                findNavController().apply {
                    for (i in 1..3)
                        popBackStack()
                }
            }

            moves.text = resources.getString(R.string.moves, gameResultViewModel.gameResult.moves)

            val diff = gameResultViewModel.gameResult.difficulty
            difficulty.text = resources.getString(R.string.difficulty, diff)
        }

        return binding.root
    }
}
