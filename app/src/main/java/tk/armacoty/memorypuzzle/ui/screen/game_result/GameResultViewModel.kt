package tk.armacoty.memorypuzzle.ui.screen.game_result

import androidx.lifecycle.ViewModel
import tk.armacoty.memorypuzzle.model.entity.GameResult

class GameResultViewModel(
    val gameResult: GameResult
) : ViewModel()
