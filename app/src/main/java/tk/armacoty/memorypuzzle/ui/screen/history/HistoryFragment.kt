package tk.armacoty.memorypuzzle.ui.screen.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.koin.androidx.viewmodel.ext.android.viewModel
import tk.armacoty.memorypuzzle.R
import tk.armacoty.memorypuzzle.databinding.HistoryFragmentBinding
import tk.armacoty.memorypuzzle.ui.screen.history.adapter.HistoryItemAdapter

class HistoryFragment : Fragment() {
    private val historyViewModel: HistoryViewModel by viewModel()

    private lateinit var binding: HistoryFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.history_fragment,
            container,
            false
        )

        with(binding) {
            val adapter = HistoryItemAdapter()
            rows.adapter = adapter

            historyViewModel.history.observe(
                viewLifecycleOwner
            ) { history ->
                history?.let {
                    adapter.data = history
                }
            }

            toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            }
        }

        return binding.root
    }
}
