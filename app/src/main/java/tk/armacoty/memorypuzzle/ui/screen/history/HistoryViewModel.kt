package tk.armacoty.memorypuzzle.ui.screen.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import tk.armacoty.memorypuzzle.model.database.dao.GameResultDao
import tk.armacoty.memorypuzzle.model.entity.GameResult

class HistoryViewModel(
    private val gameResultDao: GameResultDao
) : ViewModel() {
    private val _history = MutableLiveData<List<GameResult>>()
    val history: LiveData<List<GameResult>>
        get() = _history

    init {
        _history.value = listOf()

        viewModelScope.launch(Dispatchers.IO) {
            _history.postValue(gameResultDao.getAll())
        }
    }
}
