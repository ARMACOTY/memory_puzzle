package tk.armacoty.memorypuzzle.ui.screen.history.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tk.armacoty.memorypuzzle.R
import tk.armacoty.memorypuzzle.model.entity.GameResult
import java.time.format.DateTimeFormatter

class HistoryItemAdapter : RecyclerView.Adapter<HistoryItemAdapter.HistoryItemViewHolder>() {
    var data = listOf<GameResult>()
        @SuppressLint("NotifyDataSetChanged")
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        from(parent)

    override fun onBindViewHolder(holder: HistoryItemViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
    }

    override fun getItemCount() = data.size

    class HistoryItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val movesView: TextView = itemView.findViewById(R.id.moves)
        private val difficultyView: TextView = itemView.findViewById(R.id.difficulty)
        private val timeView: TextView = itemView.findViewById(R.id.time)

        @SuppressLint("NewApi")
        fun bind(item: GameResult) {
            movesView.text = itemView.resources.getString(R.string.moves, item.moves)

            difficultyView.text = itemView.resources.getString(
                R.string.difficulty, item.difficulty
            )

            val formatter = DateTimeFormatter.ofPattern(
                itemView.resources.getString(R.string.history_time_format)
            )
            timeView.text = item.time.format(formatter)
        }
    }

    private fun from(parent: ViewGroup): HistoryItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(
            R.layout.history_item_layout,
            parent,
            false
        )
        return HistoryItemViewHolder(view)
    }
}
