package tk.armacoty.memorypuzzle.ui.screen.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import tk.armacoty.memorypuzzle.R
import tk.armacoty.memorypuzzle.databinding.HomeFragmentBinding

class HomeFragment : Fragment() {
    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.home_fragment,
            container,
            false
        )

        with(binding) {
            newGameButton.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionNewGame())
            }

            historyButton.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionOpenHistory())
            }
        }

        return binding.root
    }
}
