package tk.armacoty.memorypuzzle.ui.screen.new_game

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import tk.armacoty.memorypuzzle.R
import tk.armacoty.memorypuzzle.databinding.NewGameFragmentBinding
import tk.armacoty.memorypuzzle.model.entity.Difficulty

class NewGameFragment : Fragment() {
    private lateinit var binding: NewGameFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.new_game_fragment,
            container,
            false
        )

        with(binding) {
            easyButton.setOnClickListener {
                findNavController().navigate(
                    NewGameFragmentDirections.actionStartGame(Difficulty.EASY)
                )
            }

            mediumButton.setOnClickListener {
                findNavController().navigate(
                    NewGameFragmentDirections.actionStartGame(Difficulty.MEDIUM)
                )
            }

            hardButton.setOnClickListener {
                findNavController().navigate(
                    NewGameFragmentDirections.actionStartGame(Difficulty.HARD)
                )
            }
        }

        return binding.root
    }
}
