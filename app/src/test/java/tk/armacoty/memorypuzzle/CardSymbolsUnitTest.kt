package tk.armacoty.memorypuzzle

import org.junit.Assert.assertEquals
import org.junit.Test
import tk.armacoty.memorypuzzle.model.entity.CardSymbols
import tk.armacoty.memorypuzzle.model.entity.Difficulty

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class CardSymbolsUnitTest {
    @Test
    fun easySymbolsAreCorrect() {
        val symbols = CardSymbols.give(Difficulty.EASY)
        val n = 4

        assertEquals(
            symbols.size,
            n * n / 2
        )
    }

    @Test
    fun mediumSymbolsAreCorrect() {
        val symbols = CardSymbols.give(Difficulty.MEDIUM)
        val n = 8

        assertEquals(
            symbols.size,
            n * n / 2
        )
    }

    @Test
    fun hardSymbolsAreCorrect() {
        val symbols = CardSymbols.give(Difficulty.HARD)
        val n = 12

        assertEquals(
            symbols.size,
            n * n / 2
        )
    }
}
